import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'common/global.dart';
import 'my_app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(
      debug: true // optional: set false to disable printing logs to console
  );

  WidgetsFlutterBinding.ensureInitialized();
  authToken = await storage.read(key: "token");
  runApp(MyApp(authToken));
}



